#include "stdafx.h"
#include "Shape.h"
#include "Field.h"
#include "Utility.h"

Shape::Shape(int _speed){
	shapeType = static_cast<ShapeType>(Utility::getRandomInt(1, 7));

	//shapeType = ShapeType::LShapeB;
	rotation = 1;
	speed = _speed;

	//try to always start block top center
	xPos = FIELDX + ((BLOCKSPERROW * BLOCKWIDTH) / 2) - (BLOCKWIDTH/2);
	yPos = FIELDY + (BLOCKSPERCOL * BLOCKHEIGHT) - (BLOCKHEIGHT * 4);

	fillShapeData();
	
	fillBlocks();

	lastGravityTick = GetTickCount();
	lastMoveTick = GetTickCount();
	lastRotateTick = GetTickCount();
}

Shape::Shape(int shapeID, int color){}

void Shape::start(){}

void Shape::update(int direction){

	if (
		(direction == 2 || direction == 3 || direction == 4) 
		&& (GetTickCount() > lastMoveTick + (100 / speed))
		)
	{
		moveShape(direction);
		lastMoveTick = GetTickCount();
	}
	else if (direction == 5 && GetTickCount() > lastRotateTick + 150) {
		moveShape(direction);
		lastRotateTick = GetTickCount();
	}
	else if (GetTickCount() > lastGravityTick + (1000 / speed))
	{
		moveShape(3);
		lastGravityTick = GetTickCount();
	}
}

void Shape::draw(){

	for (int x = 0; x <= 3; x++)
	{
		for (int y = 0; y <= 3; y++)
		{
			if (blocks[x][y]->color>=0)
				blocks[x][y]->draw();
		}
	}
}

 void Shape::rotate(int direction){
	
	if (shapeType == ShapeType::Square)
		rotation = 1; // force no rotation
	else if (shapeType == ShapeType::Line || shapeType == ShapeType::ZShape || shapeType == ShapeType::ZShapeB){
		if (rotation == 1)
			rotation = 2;
		else if (rotation == 2)
			rotation = 1;
	}
	else{

		if (direction == 1){
			if (rotation == 1)
			{
				rotation = 4;
			}
			else
				rotation--;
		}
		else if (direction == 2){
			if (rotation == 4)
				rotation = 1;
			else
				rotation++;
		}
	}
}

void Shape::fillShapeData(){
	// shapeData[5][3][3][3]
	for (int x = 0; x < 7; x++)
	{
		for (int y = 0; y <= 3; y++)
		{
			for (int z = 0; z <= 3; z++)
			{
				for (int k = 0; k <= 3; k++)
				{
					shapeData[x][y][z][k] = false;
				}
			}
		}
	}

	// stick
	// rotation 0
	shapeData[0][0][0][0] = true;
	shapeData[0][0][0][1] = true;
	shapeData[0][0][0][2] = true;
	shapeData[0][0][0][3] = true;

	// rotation 1
	shapeData[0][1][0][0] = true;
	shapeData[0][1][1][0] = true;
	shapeData[0][1][2][0] = true;
	shapeData[0][1][3][0] = true;
	// square
	// rotation 0
	shapeData[1][0][0][0] = true;
	shapeData[1][0][1][0] = true;
	shapeData[1][0][0][1] = true;
	shapeData[1][0][1][1] = true;
	// L shape
	// rotation 0
	shapeData[2][0][0][0] = true;
	shapeData[2][0][1][0] = true;
	shapeData[2][0][0][1] = true;
	shapeData[2][0][0][2] = true;
	// rotation 1
	shapeData[2][1][0][0] = true;
	shapeData[2][1][0][1] = true;
	shapeData[2][1][1][1] = true;
	shapeData[2][1][2][1] = true;
	// rotation 2
	shapeData[2][2][1][0] = true;
	shapeData[2][2][1][1] = true;
	shapeData[2][2][0][2] = true;
	shapeData[2][2][1][2] = true;
	// rotation 3
	shapeData[2][3][0][0] = true;
	shapeData[2][3][1][0] = true;
	shapeData[2][3][2][0] = true;
	shapeData[2][3][2][1] = true;

	// L shape Backwards
	// rotation 0
	shapeData[3][0][0][0] = true;
	shapeData[3][0][1][0] = true;
	shapeData[3][0][1][1] = true;
	shapeData[3][0][1][2] = true;
	// rotation 1
	shapeData[3][1][0][0] = true;
	shapeData[3][1][1][0] = true;
	shapeData[3][1][2][0] = true;
	shapeData[3][1][0][1] = true;
	// rotation 2
	shapeData[3][2][0][0] = true;
	shapeData[3][2][0][1] = true;
	shapeData[3][2][0][2] = true;
	shapeData[3][2][1][2] = true;
	// rotation 3
	shapeData[3][3][2][0] = true;
	shapeData[3][3][0][1] = true;
	shapeData[3][3][1][1] = true;
	shapeData[3][3][2][1] = true;

	// z shape
	// rotation 0
	shapeData[4][0][1][0] = true;
	shapeData[4][0][2][0] = true;
	shapeData[4][0][0][1] = true;
	shapeData[4][0][1][1] = true;

	// rotation 1
	shapeData[4][1][0][0] = true;
	shapeData[4][1][0][1] = true;
	shapeData[4][1][1][1] = true;
	shapeData[4][1][1][2] = true;

	// z shape backwards
	// rotation 0
	shapeData[5][0][0][0] = true;
	shapeData[5][0][1][0] = true;
	shapeData[5][0][1][1] = true;
	shapeData[5][0][2][1] = true;

	// rotation 1
	shapeData[5][1][1][0] = true;
	shapeData[5][1][0][1] = true;
	shapeData[5][1][1][1] = true;
	shapeData[5][1][0][2] = true;

	// T SHape
	// rotation 0
	shapeData[6][0][0][0] = true;
	shapeData[6][0][1][0] = true;
	shapeData[6][0][2][0] = true;
	shapeData[6][0][1][1] = true;
	// rotation 1
	shapeData[6][1][0][0] = true;
	shapeData[6][1][0][1] = true;
	shapeData[6][1][1][1] = true;
	shapeData[6][1][0][2] = true;
	// rotation 2
	shapeData[6][2][1][0] = true;
	shapeData[6][2][0][1] = true;
	shapeData[6][2][1][1] = true;
	shapeData[6][2][1][2] = true;
	// rotation 3
	shapeData[6][3][1][0] = true;
	shapeData[6][3][0][1] = true;
	shapeData[6][3][1][1] = true;
	shapeData[6][3][1][2] = true;

}

void Shape::fillBlocks(){
	for (int x = 0; x <= 3; x++)
	{
		for (int y = 0; y <= 3; y++)
		{
			blocks[x][y] = new  Block();
			if (shapeData[static_cast<int>(shapeType) - 1][rotation - 1][x][y])
			{
				blocks[x][y]->xPos = xPos + (x * BLOCKWIDTH);
				blocks[x][y]->yPos = yPos + (y * BLOCKHEIGHT);
				blocks[x][y]->color = static_cast<int>(shapeType)+1; // for now, fuck it
			}
		}
	}
}

void Shape::moveShape(int direction){
	//RIGHT
	if (direction == 2){
		xPos += BLOCKWIDTH;
	}
	//LEFT
	if (direction == 4){
		xPos -= BLOCKWIDTH;
	}

	//DOWN
	if (direction == 3)
		yPos -= BLOCKHEIGHT;

	//Counter Clockwise
	if (direction == 5){
		rotate(1);
	}

	fillBlocks();
}

Shape::~Shape()
{
}
