#pragma once
#include "block.h"

class Shape
{
public:
	enum ShapeType{
		Line = 1,
		Square = 2,
		LShape = 3,
		LShapeB = 4,
		ZShape = 5,
		ZShapeB = 6,
		TShape = 7
	};

	ShapeType shapeType;
	int xPos;
	int yPos;
	int color;
	int rotation; // 0,1
	int speed;
	Block *blocks[4][4];
	bool shapeData[7][4][4][4];

	void rotate(int direction);
	void fillBlocks();

	void start();
	void update(int direction);
	void draw();

	Shape(int _shapeID, int _color);
	Shape(int speed);
	~Shape();

	


private:
	int lastGravityTick;
	//int baseLastGravityTick = 300;
	int lastMoveTick;
	int lastRotateTick;
	void fillShapeData();
	void moveShape(int direction);


};



