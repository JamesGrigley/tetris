#pragma once
#include "Shape.h"

class HUD
{
public:

	Shape nextShape();

	HUD();
	~HUD();

	void update();
	void draw();
	
};

