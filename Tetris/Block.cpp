#include "stdafx.h"
#include "Block.h"
#include "Field.h"

#include "Utility.h"

Block::Block(){
	xPos = -1;
	yPos = -1;
	color = -1;
}

Block::Block(int _x, int _y){
	xPos = _x;
	yPos = _y;
	color = -1;
}

Block::Block(int _color){
	color = _color;
}

Block::~Block()
{
}

void Block::start(){

}

void Block::update(){

}

void Block::draw(){
	Utility::drawRect(xPos, yPos, BLOCKWIDTH, BLOCKWIDTH, color);
	Utility::drawRectBorder(xPos, yPos, BLOCKWIDTH, BLOCKWIDTH, 2, 0);
}

