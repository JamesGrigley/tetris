#include "stdafx.h"
#include "Utility.h"
#include <random>

void Utility::drawRect(int x, int y, int w, int h, int color) {
	ColorRGB* colorRGB = new ColorRGB(color);
	glColor3f(colorRGB->r, colorRGB->g, colorRGB->b);
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + w, y);
	glVertex2f(x + w, y + h);
	glVertex2f(x, y + h);
	glEnd();


	//drawText(x + 2, y + 2, int2str(position));
}

void Utility::drawRectBorder(int x, int y, int w, int h, int borderWidth, int color) {
	//drawRect(x, y + h - borderWidth, w, borderWidth, color);//top
	drawRect(x+w - borderWidth, y, borderWidth, h, color);//right
	drawRect(x, y, w, borderWidth, color);//bottom
	//drawRect(x, y, borderWidth, h, color);//left
}

int Utility::getRandomInt(int lowerRange, int upperRange){
	std::default_random_engine generator((std::random_device())()); // create and seed the source of randomness
	std::uniform_int_distribution<int> distribution(lowerRange, upperRange);
	return distribution(generator);  // generates number in the range 
}