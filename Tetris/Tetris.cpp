#include "stdafx.h"
#include "Field.h"
#pragma comment(lib, "Opengl32.lib")
#define VK_A 0x41
#define VK_D 0x44
#define VK_S 0x53
#define VK_W 0x57
#define VK_SPACE 0x20


// window size and update rate (60 fps)
int width = 1600;
int height = 900;
int interval = 1000 / 60;

Field *field;

//START
void start()
{
	field = new Field();
	field->start();
}

void keyboard() {
	//if (GetAsyncKeyState(VK_W))
	//	field->update()
	if (GetAsyncKeyState(VK_SPACE)) //space
		field->update(5);
	if (GetAsyncKeyState(VK_D)) //right
		field->update(2);
	if (GetAsyncKeyState(VK_A)) //left
		field->update(4);
	if (GetAsyncKeyState(VK_S)) // down
		field->update(3);
}

void update(int i){
	// do updates here
	
	// input handling
	keyboard();

	field->update(0);

	// Call update() again in 'interval' milliseconds
	glutTimerFunc(interval, update, 0);

	// Redisplay frame
	glutPostRedisplay();
}

void draw(){// clear (has to be done at the beginning)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	field->draw();

	glutSwapBuffers();
}

void enable2D(int width, int height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, width, 0.0f, height, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// program entry point
int _tmain(int argc, char** argv) {

	// initialize opengl (via glut)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutCreateWindow("Tetris");

	// Register callback functions  
	start();
	glutDisplayFunc(draw);
	glutTimerFunc(interval, update, 0);

	// setup scene to 2d mode and set draw color to white
	enable2D(width, height);
	glColor3f(1.0f, 0.0f, 1.0f);

	// start the whole thing
	glutMainLoop();
	return 0;
}