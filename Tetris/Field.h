#pragma once
#include "stdafx.h"
#include "block.h"
#include "shape.h"

#define BLOCKWIDTH 30
#define BLOCKHEIGHT 30
#define BLOCKSPERROW 15
#define BLOCKSPERCOL 25
#define FIELDX 400
#define FIELDY 100

class Field
{
private:
	Block *fieldBlocks[BLOCKSPERROW][BLOCKSPERCOL];
	Shape *currentShape;
	Shape *nextShape;

	enum CollisionType{
		None = 0,
		Down = 1,
		Side = 2
	};

	void addShapeToBlocks();
	void addBlock(int _x, int _y, int _color);
	void drawBlocks();
	void loadBlocks();
	int checkCollision(int direction);
public:
	int speed;
	Field();
	~Field();

	void addShape();

	int collidesWithField(int _x, int _y, int direction);
	
	void shiftFieldBlocks(int y, int rowCount);
	void clearLines(int(&lines)[4]);
	void checkLines();

	void start();

	void update(int direction);
	
	void draw();
};

